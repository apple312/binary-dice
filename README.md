# Binary Dice

Small Embedded C "bare metal" project. Using accelerometer and few LEDs to simulate dice. [KL05Z, Cortex M0+].

You can find report about project in [**this file**](https://gitlab.com/apple312/binary-dice/blob/master/REPORT_TM2_%C5%81UKASZ_HAJEC.pdf) [POLISH].

There you can see a [**demo video**](https://drive.google.com/file/d/1ho8h3NLYlUGuubqdRlZNaYzM9KB35jpX/view).


![small](device.jpg)


*Project was made for Microprocessor Techniques course at university.*