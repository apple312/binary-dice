#include "MKL05Z4.h"
#include "sevseg.h"

void sevseg_init(void)
{
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;
	PORTA->PCR[0] = PORT_PCR_MUX(1UL);
	PORTA->PCR[5] = PORT_PCR_MUX(1UL);
	PORTA->PCR[6] = PORT_PCR_MUX(1UL);
	PORTA->PCR[7] = PORT_PCR_MUX(1UL);
	PORTA->PCR[8] = PORT_PCR_MUX(1UL);
	PORTA->PCR[11] = PORT_PCR_MUX(1UL);
	PORTA->PCR[12] = PORT_PCR_MUX(1UL);
	
	PTA->PDDR |= 1 << 12 | 1 << 11 | 1 << 8 | 1 << 7 | 1 << 6 | 1 << 5 | 1 << 0;
	PTA->PCOR = A_LED | B_LED | C_LED |D_LED | E_LED | F_LED | G_LED;
}

void sevseg_digit(void)
{
	PTA->PSOR = A_LED | B_LED;
}
