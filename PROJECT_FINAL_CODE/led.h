#include "MKL05Z4.h"

#define BLUE				1<<10
#define GREEN				1<<9
#define RED					1<<8

#define GREENB			1<<7
#define YELLOWB			1<<6
#define REDB				1<<5

void led_init(void);
void rgb_one_led(uint32_t color);
void digit_led(uint8_t digit);
