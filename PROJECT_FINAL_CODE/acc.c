#include "MKL05Z4.h"
#include "acc.h"
#include "stdint.h"
#include "iic.h"

#define FT_MT_THS_1_REG       0x17
void MMA8451WriteRegister(uint8_t address, uint8_t reg,uint8_t val);
uint8_t u8MMA8451ReadRegister(uint8_t address, uint8_t reg);

void MMA8451_Read_3axis_data(void);
uint16_t  resultx, resulty, resultz;

/*********************************************************\
* I2C Write Register
\*********************************************************/
void MMA8451WriteRegister(uint8_t address, uint8_t reg,uint8_t val)
{
  //I2C0_C1_TX = 1;                                // Transmit Mode
  I2C0->C1 |= I2C_C1_TX_MASK; 
  I2C_Start();                                  // Send Start
  I2C_CycleWrite(address);                      // Send I2C "Write" Address
  I2C_CycleWrite(reg);                          // Send Register
  I2C_CycleWrite(val);                          // Send Value
  I2C_Stop();                                   // Send Stop
}


/*********************************************************\
* I2C Read Register
\*********************************************************/
uint8_t u8MMA8451ReadRegister(uint8_t address, uint8_t reg)
{
  uint8_t b;
  //I2C_C1_TX = 1;                                // Transmit Mode
  I2C0->C1 |= I2C_C1_TX_MASK; 
  I2C_Start();                                  // Send Start
  I2C_CycleWrite(address);                      // Send I2C "Write" Address
  I2C_CycleWrite(reg);                          // Send Register
  I2C_RepeatStart();                            // Send Repeat Start
  I2C_CycleWrite(address+1);                    // Send I2C "Read" Address
  b = I2C_CycleRead(1);                         // *** Dummy read: reads "I2C_ReadAddress" value ***
  b = I2C_CycleRead(1);                         // Read Register Value
  I2C_Stop();                                   // Send Stop
  return b;
}

/*************************** MY FUNCTIONS *******************************/
void acc_read_data(void)
{
    {
				//values from acc are 14bit resolution
        resultx   = u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, OUT_X_MSB_REG)<<8;
        resultx  |= u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, OUT_X_LSB_REG);
        resultx >>= 2;

        resulty   = u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, OUT_Y_MSB_REG)<<8;
        resulty  |= u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, OUT_Y_LSB_REG);
        resulty >>= 2;

        resultz   = u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, OUT_Z_MSB_REG)<<8;
        resultz  |= u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, OUT_Z_MSB_REG);
        resultz >>= 2;
    }
}


void acc_init(void)
{
	uint8_t status;
	unsigned char reg_val = 0;
	
	// enable PORTA and PORTB clocks
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK; 
  SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	
	//reset all acc registers
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, CTRL_REG2, BOOT_MASK);
	
	// wait for the RST bit to clear
	do    
	{
		reg_val = u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, CTRL_REG2) & BOOT_MASK;
	}while (reg_val);
			
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, XYZ_DATA_CFG_REG, FULL_SCALE_8G); 															// set acc scale to 8g		 
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, CTRL_REG1,  0x78);//DR1_MASK | DR2_MASK);											  				// set ODR = 100HZ and standby mode (BYLO 0x78)!!!!			
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, FF_MT_CFG_1_REG, 0x78); //OAE_MASK | ZEFE_MASK | YEFE_MASK | XEFE_MASK);		
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, FF_MT_THS_1_REG, 0x12); 																				// threshold setting value for the motion detection (0.5g)		
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, FF_MT_COUNT_1_REG, 0x12);																				// set the debounce counter to 110 ms timer
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, CTRL_REG4, INT_EN_FF_MT_1_MASK);																// enable Motion/Freefall interrupt		
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, CTRL_REG5, 0xFB); 																							// route all other interrupts to pin INT1
	
	// turn to acc to active mode
	status = u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, CTRL_REG1); 
	MMA8451WriteRegister(MMA8451_I2C_ADDRESS, CTRL_REG1, status | 1);
	
	//enable interrupt for INT2 - PA10
	NVIC_EnableIRQ(PORTA_IRQn);
	NVIC_ClearPendingIRQ(PORTA_IRQn);	
	PORTA->PCR[10] = PORT_PCR_IRQC(10);
	PORTA->PCR[10] |= 1<<8; 							// MUX
	PORTA->PCR[10] |= PORT_PCR_ISF_MASK;  // to disable instant interrupt
}


