#include "MKL05Z4.h"
#include "led.h"
#include "stdint.h"
#include "stdlib.h"
#include "iic.h"
#include "acc.h"

//1 there was an interrupt or we are still doing it, 0 wait for one
int interupt_flag = 0; 
uint16_t throw_number = 0;

void PORTA_IRQHandler(void)
{
	uint8_t source = 0;
	
	// to stop another the same interrupt
	PORTA->PCR[10] |= PORT_PCR_ISF_MASK;
	
	//check source of interrupt
	source = u8MMA8451ReadRegister(MMA8451_I2C_ADDRESS, INT_SOURCE_REG);
	if( (source & SRC_FF_MT_1_MASK) == SRC_FF_MT_1_MASK )
	{
		if( interupt_flag == 0 )
			interupt_flag = 1;				
	}
	
	//read position registers and use it as a seed
	acc_read_data();
}

int main(void)
{
	led_init();
	rgb_one_led(BLUE);
	iic_init();
	acc_init();
	
	while(1)
	{
		if(interupt_flag == 1)
		{
			int i = 40000000;
			int x;
			srand( (resultx | resulty | resultz) + throw_number );
	
			x = rand();
			x = x % 6 + 1;
			digit_led(x);
			
			// wait before next throw
			rgb_one_led(RED);
			while(i)
			{
				i--;
			}
			
			// next throw possible
			interupt_flag = 0;
			rgb_one_led(GREEN);
		}		
	}
}

