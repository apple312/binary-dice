#include "MKL05Z4.h"
#include "led.h"


void led_init(void)
{
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;
	
	PORTB->PCR[8] = PORT_PCR_MUX(1UL);
	PORTB->PCR[9] = PORT_PCR_MUX(1UL);
	PORTB->PCR[10] = PORT_PCR_MUX(1UL);
	
	PORTB->PCR[6] = PORT_PCR_MUX(1UL);
	PORTB->PCR[7] = PORT_PCR_MUX(1UL);
	PORTB->PCR[5] = PORT_PCR_MUX(1UL);
		
	PTB->PDDR |= 1 << 10 | 1 << 9 | 1 << 8 | 1 << 7 | 1 << 6 | 1 << 5;
}

void rgb_one_led(uint32_t color)
{
	PTB->PSOR = BLUE | GREEN | RED;
	PTB->PCOR = color;	
}

void digit_led(uint8_t digit)
{
	PTB->PCOR = YELLOWB | GREENB | REDB;
	switch(digit)
	{
		case 0:
			break;
		case 1:
			PTB->PSOR = GREENB;
			break;
		case 2:
			PTB->PSOR = YELLOWB;
			break;
		case 3:
			PTB->PSOR = YELLOWB | GREENB;
			break;
		case 4:
			PTB->PSOR = REDB;
			break;
		case 5:
			PTB->PSOR = GREENB | REDB;
			break;
		case 6:
			PTB->PSOR = YELLOWB | REDB;
			break;
		case 7:
		default:
			PTB->PSOR = YELLOWB | GREENB | REDB;
			break;
	}
}

	